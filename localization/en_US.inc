<?php
$labels = array();
$labels['bookmarks'] = 'Bookmarks';
$labels['bookmarks_new'] = 'Add new URL';
$labels['bookmarks_url'] = 'Please enter a new URL:';
$labels['bookmarks_del'] = 'Should the bookmark "%b%" really be deleted?';
?>