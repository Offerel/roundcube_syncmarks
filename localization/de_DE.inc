<?php
$labels = array();
$labels['bookmarks'] = 'Bookmarks';
$labels['bookmarks_new'] = 'Neuen URL hinzufügen';
$labels['bookmarks_url'] = 'Bitte den neuen URL eingeben:';
$labels['bookmarks_del'] = 'Soll das Bookmark "%b%" wirklich gelöscht werden?';
?>